﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace CICDDemo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GetVariableController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        public GetVariableController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpGet]
        public string Get()
        {
            return _configuration.GetValue<string>("MyVariable");
        }
    }
}